const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
var buildConfig = require('./build.config.js');

const outputLocation = 'build/release'

module.exports = {
	mode: 'production',
	entry: path.resolve(__dirname, '../src/index.js'),
	output: {
		filename: 'js/[name].js',
		chunkFilename: 'js/[id].js',
		path: path.resolve(__dirname, "../" + outputLocation),
	},

	optimization: {
		splitChunks: {
			cacheGroups: {
				commons: {
					test: /[\\/]node_modules[\\/]/,
					name: "vendor",
					chunks: "all"
				}
			}
		},
		minimize: true
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: buildConfig.getGameTitle(),
			filename: 'index.html',
			template: './src/index.html',
			externals: buildConfig.getExternalOutputPaths('external/')
		}),
		new CopyWebpackPlugin([{
			from: './assets/',
			to: 'assets/',
			toType: 'dir',
			cache: true
		},
		{
			from: { glob: buildConfig.getExternalLocation() + "/?(" + buildConfig.getExternals().join("|") + ")", dot: true },
			to: 'external/',
			toType: 'dir',
			flatten: true,
			cache: true
		}
		]),
		new CleanWebpackPlugin([outputLocation], {
			root: path.resolve(__dirname, '..')
		}),

	]
};