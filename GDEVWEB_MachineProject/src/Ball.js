import { Container, Text,TEXT_GRADIENT,ticker,utils,Sprite } from "pixi.js";
class Ball extends Container{
    constructor(xPos,yPos,radius,color,angle){
		super();
		this.xOrigin = xPos;
		this.yOrigin = yPos;
        this.bRadius = radius;
        this.bColor = color;
        this.bAngle = angle;
        this.xProjectile = Math.cos(this.degToRad(this.bAngle));
        this.yProjectile = Math.sin(this.degToRad(this.bAngle));
        this.speed = 10;
		//console.log("w = "+this.eWidth+", h = "+this.eHeight);
		//console.log("x = "+this.xOrigin+", y = "+this.yOrigin);
		this.circle = new PIXI.Graphics();
		this.circle.beginFill(this.bColor);
		this.circle.drawCircle(this.xOrigin,this.yOrigin,this.bRadius);
		this.circle.endFill();
		
		this.addChild(this.circle);
		
	}
	UpdateBall(){
        this.circle.x += this.speed * this.xProjectile;
		this.circle.y += -1*this.speed * this.yProjectile;
		var cirX = this.circle.x+this.xOrigin;
		var cirY = this.circle.y+this.yOrigin;
		if(this.circle.x+this.xOrigin<=30||this.circle.x+this.xOrigin>=540)
		{
			this.xProjectile *= -1;
		}
		//cirY*=-1;
		//console.log("x = "+this.retXPosition()+", y = "+this.retYPosition());
	}
    retXPosition()
	{
		var xDis = this.xOrigin+this.circle.x;
		return xDis;
    }
    retYPosition()
	{
		var yDis = this.yOrigin+this.circle.y;
		return yDis;
	}
    degToRad(angle)
    {
        return angle * (Math.PI/180);
	}
	retColor()
	{
		return this.bColor;
	}
	setTheCirclePosition(xPos,yPos)
	{
		this.circle.x = xPos-this.xOrigin;
		this.circle.y = yPos-this.yOrigin;
		console.log("setBX = "+xPos+" setBY = "+yPos);
	}
}
export default Ball;