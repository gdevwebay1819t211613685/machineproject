import { Container, Text,TEXT_GRADIENT,ticker,utils,Sprite } from "pixi.js";
import KeyCodes from "../KeyCodes";
import KeyValues from "../KeyValues";
import Ball from "../Ball";

class TitleScreen extends Container{
    constructor(){
        super();
        this.interactive = true;
        this.playingBGM = true;
        this.ticker = ticker.shared;
        this.ticker.add(this.update,this);
        this.elapsedTime = 0;
        this.scoreCounter = 0;
        this.turnsLeft = 10;
        this.xOrigin=277.5;
        this.yOrigin=700;
        this.ballRadius = 25;
        this.angle = 5;
        this.colCounter = 0;
        this.isOkayToShoot = true;
        this.isOkayToCheckCollision = true;
        this.isStillOkayToPlay = true;
        this.colorQueue = [];
        this.currentBall = [];
        this.balls = [];
        this.allColors = [0x0000FF,0x00FF00,0xFFC0CB,0xFFA500,0x800080,0xA52A2A];
        //Blue= 0x0000FF Green=0x00FF00 Pink=0xFFC0CB 
        //Orange=0xFFA500 Purple=0x800080 Brown=0xA52A2A
        //the 2d array
        this.hexGrid = [];

        for(let v = 0; v<4;v++)
        {
            this.colorQueue.push(this.allColors[this.colorIndexRandomizer()]);
        }
        
        console.log("Colors =  "+ this.colorQueue);

        this.scoreText = new PIXI.Text("Score = ",{
            fontFamily: 'Arial',
            fontSize: 20,
            fill: 0xFFFFFF,
            align: 'center'
        });
        this.scoreText.position.set(350, 730);
        this.bgmInstance = createjs.Sound.play("assets/audio/Bgm.mp3",{
            volume : 0.5,
            loop : -1
            //delay: 100 //ma
        })

        this.addChild(this.scoreText);

        this.turnsText = new PIXI.Text("Score = ",{
            fontFamily: 'Arial',
            fontSize: 20,
            fill: 0xFFFFFF,
            align: 'center'
        });
        this.turnsText.position.set(200, 730);
        
        this.addChild(this.turnsText);

        //for collision testing purposes
        //this.eball = new Ball(300,300,this.ballRadius,this.colorQueue[2],this.angle);
        //this.addChild(this.eball);
        //this.balls.push(this.eball);
        
        this.spawnGrid();
        this.getGridCoordinate();
        this.spawnLevel();
        
        //console.log("x = "+this.eball.retXPosition()+ " y = "+this.eball.retYPosition());
/*
        this.circle = new PIXI.Graphics();
		this.circle.beginFill(0xFF0000);
		this.circle.drawCircle(this.xOrigin,this.yOrigin,15);
        this.circle.endFill();
		
        this.addChild(this.circle);
*/       
        this.showBallInQueue();

        window.addEventListener("keyup",this.onKeyUp.bind(this));
        window.addEventListener("mousedown",this.mouseDown.bind(this));
        window.addEventListener("mousemove",this.mouseMove.bind(this));
        this.ticker.add(this.updateObjs,this);
    }
    mouseDown(m){

       
        if(this.isOkayToShoot == true && this.isStillOkayToPlay == true)
        {
            this.turnsLeft--;
            //spawns the ball
            this.ball = new Ball(this.xOrigin,this.yOrigin,this.ballRadius,this.colorQueue[0],this.angle);
            this.addChild(this.ball);
            //pushes the ball to currentball array to make isOkayToPlay false
            this.currentBall.push(this.ball);
            console.log("Array =  "+ this.currentBall.length);
            this.isOkayToShoot = false;
            console.log("isOkayToShoot =  "+ this.isOkayToShoot);
            this.removeChild(this.colorQueue[0]);
            this.colorQueue.shift();
            console.log("colorarray =  "+ this.colorQueue.length);
            //for the color queue display
            this.addColorToColorQueue();
            this.showBallInQueue();
        }
        //console.log("Clicked!!!!!!!!!!!"+"x =  "+this.xProj+" y = "+this.yProj+" Angle = "+this.angle);
    }
    //updates the angle
    mouseMove(p){
        var hori = this.xOrigin - p.x;
        var verti = this.yOrigin - p.y+15;
        hori *= -1;
        var rad = Math.atan2(verti,hori);
        this.angle = this.radToDeg(rad);
        //this.xProj = Math.cos(this.degToRad(this.angle));
        //this.yProj = Math.sin(this.degToRad(this.angle));
        /*
        if(this.angle<0)
        {
            this.angle += 180; 
        }
        */
        //console.log("x =  "+ hori+" y = "+verti+" Angle = "+this.angle);
    }
    onKeyUp(e){

    }
    updateObjs()
    {
        //the is okay to shoot is based on the current ball array
        if(this.currentBall.length<=0)
        {
            this.isOkayToShoot = true;
        }
        //updates the current balls position
        this.updateObjectMovements()
        //for testing purposes, when circle.x is more that 0, circle will be deleted
        if(this.currentBall.length>0)
        {
            this.checkPosition();
        }
        this.checkCollision();
        if (this.turnsLeft <= 0) 
        {
            var gameOver = new Text("Game Over",{
                fontSize : 40,
                fill : 0xffffff
                });
            gameOver.position.set(300, 700);
            this.addChild(gameOver);
        }

        this.turnsText.text = ("Turns Left: " + this.turnsLeft);
        if (this.turnsLeft <= 0) 
        {
            this.isStillOkayToPlay = false;
        }
        //console.log("Time =  "+ this.time);
        //console.log("Enem =  "+ this.enemyArray.length);
        //console.log("pla =  "+ this.playerArray.length);
    }
    updateObjectMovements()
    {
        if(this.currentBall.length>0)
        {
            this.currentBall[0].UpdateBall();
        }
        
    }
    radToDeg(angle)
    {
        return angle * (180/Math.PI);
    }
    degToRad(angle)
    {
        return angle * (Math.PI/180);
    }
    //this is nothing just for testing purposes
    projectileMotion(obj){
        obj.x += this.speed * this.cosAng;
        obj.y += -1*this.speed * this.sinAng;
    }
    //shows the color queueu
    showBallInQueue()
    {
        this.disBall1 = new PIXI.Graphics();
		this.disBall1.beginFill(this.colorQueue[0]);
		this.disBall1.drawCircle(this.xOrigin,this.yOrigin,25);
        this.disBall1.endFill();

        this.disBall2 = new PIXI.Graphics();
		this.disBall2.beginFill(this.colorQueue[1]);
		this.disBall2.drawCircle(this.xOrigin-50,this.yOrigin,5);
        this.disBall2.endFill();

        this.disBall3 = new PIXI.Graphics();
		this.disBall3.beginFill(this.colorQueue[2]);
		this.disBall3.drawCircle(this.xOrigin-100,this.yOrigin,5);
        this.disBall3.endFill();

        this.disBall4 = new PIXI.Graphics();
		this.disBall4.beginFill(this.colorQueue[3]);
		this.disBall4.drawCircle(this.xOrigin-150,this.yOrigin,5);
        this.disBall4.endFill();
		
        this.addChild(this.disBall1);
        this.addChild(this.disBall2);
        this.addChild(this.disBall3);
        this.addChild(this.disBall4);
    }
    colorIndexRandomizer()
    {
        //generates random int for the circle only
        var randomDec = Math.random() * (this.allColors.length-1);
        var randInt = Math.floor(randomDec);
        return randInt;
    }
    addColorToColorQueue()
    {
        //add color to the queue, color not the circle
        this.colorQueue.push(this.allColors[this.colorIndexRandomizer()]);
    }
    checkCollision()
    {
        if(this.currentBall.length>0)
        {
            for(let q = 0;q<this.balls.length;q++)
            {
                var isCollide = this.onCollision(this.currentBall[0],this.balls[q]);
                console.log("Collide? =  "+ isCollide);
                //console.log("1 = "+this.currentBall[0].retColor()+ " 2 = "+this.balls[q].retColor());
                //console.log("ax =  "+ this.currentBall[0].retXPosition() + " ay = "+this.currentBall[0].retYPosition()+ " bx = "+this.balls[q].retXPosition()+" by = "+this.balls[q].retYPosition());
                if(isCollide)
                {
                    //For collision testing purposes;
                    //this.removeChild(this.currentBall[0]);
                    //if it collided to balls array current ball will transfer to the balls array
                    this.getNearestGrid(this.currentBall[0]);
                    this.balls.push(this.currentBall[0]);
                    //if currentball array is empty the isOkayToPlay will become true
                    this.currentBall.pop();
                    //return because the current ball is already transferred to the balls array
                    return;
                    //console.log("currentBall Length = "+this.currentBall.length);
                    //console.log("theballs Length = "+this.balls.length);
                }
            }
        }
        else if(this.currentBall.length<0)
        {
            return;
        }
        /*
        //This checks all the This.balls array collision
        for(let i = 0;i<this.balls.length;i++)
        {
            for(let x = 0;x<this.balls.length;x++)
            {
                this.onCollision(this.balls[i],this.balls[x+1]);
            }
        }*/
    }

    spawnGrid()
    {
        //18
        for (var j = 1 ; j <= 18; j++) 
        {
            //10
             for (var i = 1; i <= 10; i++) 
                 {
                    var k, l = 4;
                    if (j % 2 == 0) 
                    {
                        k = this.ballRadius+5;
                    }
                    else if (j %2 != 0) 
                    {
                        k = 5;
                    }
                    //this.ball = new Ball(i*(this.ballRadius*2) + k, (this.ballRadius*(j*2)) - (l*j),this.ballRadius, this.allColors[randomColor], 0, 0, 0)
                    //this.addChild(this.ball);
                    //this.balls.push(this.ball);
                    var pos = [i*(this.ballRadius*2.) + k,(this.ballRadius*(j*1.89)) - (l*j)];
                    this.hexGrid.push(pos);
                    l++;
                 }
        }
       
    }
    spawnLevel()
    {
        //100
        var howManyBalls = 100;
        for(let b = 0;b<howManyBalls;b++)
        {
            var randInt = Math.floor(Math.random() * this.allColors.length);
            this.lvlBall = new Ball(this.hexGrid[b][0],this.hexGrid[b][1],this.ballRadius,this.allColors[randInt],0,0,0);
            this.addChild(this.lvlBall);
            this.balls.push(this.lvlBall);
        }
    }

    getNearestGrid(obj)
    {
        var objX = obj.retXPosition();
        var objY = obj.retYPosition();
        var nearest = 1000;
        var nearestX = objX;
        var nearestY = objY;
        for(let y = 0;y<this.hexGrid.length;y++)
        {
            var totalX = (objX-this.hexGrid[y][0])+(objX-this.hexGrid[y][0]);
            var totalY = (objY-this.hexGrid[y][1])+(objY-this.hexGrid[y][1]);
            var totalXY = Math.abs(totalX)+Math.abs(totalY);
            var totalDistance = totalXY*0.5;
            
            if(totalDistance<nearest)
            {
                nearestX =this.hexGrid[y][0];
                nearestY =this.hexGrid[y][1];
                nearest = totalDistance;
            }

            // console.log("//////////////////////////////////////////////");
            // console.log("objx = "+objX+" objy = "+objY);
            // console.log("hex x = "+this.hexGrid[y][0]+" hex y = "+this.hexGrid[y][1]);
            // console.log("nearX = "+nearestX+" nearY = "+nearestY);
            // console.log("nearest = "+nearest+" total = "+totalDistance);
            // console.log("//////////////////////////////////////////////");
        }
        //console.log("ox =  "+ obj.retXPosition() + " oy = "+obj.retYPosition());
        obj.setTheCirclePosition(nearestX,nearestY);
        //console.log("nearX = "+nearestX+" nearY = "+nearestY);
    }
    deltaTime()
    {
        return this.ticker.elapsedMS/1000;
    }
    checkPosition()
    {
        //console.log("BEnem =  "+ this.enemyArray.length);
        //console.log("Bpla =  "+ this.playerArray.length);
        
        if(this.currentBall[0].retYPosition()<=15)
        {
            this.removeChild(this.currentBall[0]);
            this.currentBall.pop();
        }

        if (this.balls[this.balls.length-1].retYPosition() >= 600) 
        {
            var gameOver = new Text("Game Over",{
                fontSize : 40,
                fill : 0xffffff
                });
            gameOver.position.set(300, 700);
            this.addChild(gameOver);
            this.isStillOkayToPlay = false;
        }
    }

    getGridCoordinate()
    {
        for (var i = 0; i < this.balls.length; i++) 
        {
            var x = this.balls[i].xOrigin;
            var y = this.balls[i].yOrigin;
            var hexGrid = [x, y];
            this.hexGrid.push(hexGrid); 
        }
        console.log("Hex = "+this.hexGrid);
    }

    onCollision(objA,objB)
    {
        var aX = objA.retXPosition();
        var aY = objA.retYPosition();
        var bX = objB.retXPosition();
        var bY = objB.retYPosition();
        var totalRadius = this.ballRadius*2;
        var totalX = (aX-bX)+(aX-bX);
        var totalY = (aY-bY)+(aY-bY);
        var totalXY = Math.abs(totalX)+Math.abs(totalY);
        var totalDistance = totalXY*0.5;
        //console.log("ax =  "+ objE.retXPosition() + " ay = "+objE.retYPosition()+ " bx = "+objP.retXPosition()+" by = "+objP.retYPosition());
        //console.log("ax =  "+ aX + " ay = "+aY+ " bx = "+bX+" by = "+bY);
        //console.log("totalx =  "+ totalX+" totaly = "+totalY+" totaldis = "+totalDistance);
        if(totalDistance<=totalRadius)
        {
            return true;
        }
        else 
        {
            return false;
        }

        if (objA.retColor() == objB.retColor()) 
        {
            console.log("same");
        }
        //console.log(objE.retXPosition());
        //console.log(objeX+" "+objeY+" "+objpX+" "+objpY);
    }

    checkNeighbors()
    {
        var neighborsoffsets = [[[1, 0], [0, 1], [-1, 1], [-1, 0], [-1, -1], [0, -1]], [[1, 0], [1, 1], [0, 1], [-1, 0], [0, -1], [1, -1]]];  

        var ballToBeChecked = this.currentBall[0];
        var ballRow = this.currentBall[0].retYPosition() % 2;
        var neighbors = [];
        var n = neighborsoffsets[ballRow];

        for(var i = 0; i < n.length; i++)
        {


            var nx = ballToBeChecked.x + n[i][0];
            var ny = ballToBeChecked.y + n[i][1];
        }

        return neighbors;

    }

}

export default TitleScreen;