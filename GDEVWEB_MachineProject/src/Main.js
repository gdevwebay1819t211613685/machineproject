import { AssetDirectory } from "./assetdirectory";
import { loaders } from "pixi.js";
import TitleScreen from "./screen/TitleScreen";
import { App } from ".";


class Main {
    static start(){
        Main.cjAudioQueue = new createjs.LoadQueue();
        createjs.Sound.alternateExtensions = ["ogg"];

        Main.cjAudioQueue.installPlugin(createjs.Sound);
        Main.cjAudioQueue.addEventListener("complete", Main.handleAudioComplete.bind(Main));

        if(AssetDirectory.audio.length > 0){
            //LOAD AUDIO                  
            let audioFiles = AssetDirectory.audio;
            let audioManifest = [];
            for(let i = 0; i < audioFiles.length; i++){
                audioManifest.push({
                    id: audioFiles[i],
                    src: audioFiles[i]
                });
            }
            Main.cjAudioQueue.loadManifest(audioManifest);
        }
        else{
            Main.handleAudioComplete();
        }
        //createjs.Sound.volume = 0.8;//Para 0.0-1.0
        //createjs.Sound.muted = false;//

    }

    static handleAudioComplete(){
        if(AssetDirectory.load.length > 0){
            //LOAD IMAGES         
            let loader = loaders.shared;
            loader.add(AssetDirectory.load);
            loader.load(Main.handleImageComplete);
        }
        else {
            Main.handleImageComplete();
        }
    }

    static handleImageComplete(){
        let screen = new TitleScreen();
        App.stage.addChild(screen);
    }
}

export default Main;