export let AssetDirectory = {
	"load": [
		"assets/fonts/Air Americana.ttf",
		"assets/fonts/AirAmericana.eot",
		"assets/fonts/AirAmericana.svg",
		"assets/fonts/AirAmericana.woff",
		"assets/fonts/AirAmericana.woff2",
		"assets/images/Asteroid.png",
		"assets/images/Coin.png",
		"assets/images/SmallPac.png"
	],
	"audio": [
		"assets/audio/Bgm.mp3",
		"assets/audio/Click.mp3",
		"assets/audio/Knock.mp3",
		"assets/audio/Pew.mp3"
	]
};